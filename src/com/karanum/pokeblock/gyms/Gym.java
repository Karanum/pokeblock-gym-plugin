package com.karanum.pokeblock.gyms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;

import com.karanum.pokeblock.gyms.files.ConfigFile;

public class Gym {

	private String name;
	private String displayName;
	private String levelCap;
	private List<String> rules;
	private Set<UUID> leaders;
	private ConfigFile config;
	private Location warpLoc;
	
	private String rewardBadge;
	private int rewardMoney;
	private List<String> rewardItems;
	private List<String> rewardCommands;
	private List<String> rewardText;
	
	public Gym(String name, ConfigFile config) {
		leaders = new HashSet<UUID>();
		
		this.name = name;
		this.config = config;
		
		displayName = config.getConfig().getString(name + ".name");
		levelCap = config.getConfig().getString(name + ".level");
		rules = config.getConfig().getStringList(name + ".rules");
		rewardBadge = config.getConfig().getString(name + ".rewards.badge");
		rewardMoney = config.getConfig().getInt(name + ".rewards.money");
		rewardItems = config.getConfig().getStringList(name + ".rewards.items");
		rewardCommands = config.getConfig().getStringList(name + ".rewards.commands");
		rewardText = config.getConfig().getStringList(name + ".rewards.text");
		for (String leader : config.getConfig().getStringList(name + ".leaders")) {
			leaders.add(UUID.fromString(leader));
		}
		
		warpLoc = null;
		if (config.getConfig().contains(name + ".warp")) {
			World w = Bukkit.getWorld(UUID.fromString(config.getConfig().getString(name + ".warp.world")));
			double x = config.getConfig().getInt(name + ".warp.x");
			double y = config.getConfig().getInt(name + ".warp.y");
			double z = config.getConfig().getInt(name + ".warp.z");
			double pitch = config.getConfig().getDouble(name + ".warp.pitch");
			double yaw = config.getConfig().getDouble(name + ".warp.yaw");
			warpLoc = new Location(w, x, y, z, (float) yaw, (float) pitch);
		}

		PokeBlockGyms.instance.getLogger().log(Level.INFO,
				"Finished loading gym \"" + name + "\" with name: " + displayName);
	}
	
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getLevelCap() {
		return levelCap;
	}
	
	public List<String> getRules() {
		return rules;
	}
	
	public String getBadge() {
		return rewardBadge;
	}
	
	public int getRewardMoney() {
		return rewardMoney;
	}
	
	public List<String> getRewardItems() {
		return rewardItems;
	}
	
	public List<String> getRewardCommands() {
		return rewardCommands;
	}
	
	public List<String> getRewardText() {
		return rewardText;
	}
	
	public Set<UUID> getLeaders() {
		return leaders;
	}
	
	public boolean isLeader(OfflinePlayer p) {
		return leaders.contains(p.getUniqueId());
	}
	
	public void addLeader(OfflinePlayer p) {
		leaders.add(p.getUniqueId());
		saveGymConfig();
	}
	
	public void removeLeader(OfflinePlayer p) {
		leaders.remove(p.getUniqueId());
		saveGymConfig();
	}
	
	public void setWarpLocation(Location loc) {
		warpLoc = loc;
		saveGymConfig();
	}
	
	public Location getWarpLocation() {
		return warpLoc;
	}
	
	public void saveGymConfig() {
		List<String> leaderList = new ArrayList<String>();
		for (UUID leader : leaders) {
			leaderList.add(leader.toString());
		}
		config.getConfig().set(name + ".leaders", leaderList);
		
		if (warpLoc != null) {
			config.getConfig().set(name + ".warp.x", warpLoc.getBlockX());
			config.getConfig().set(name + ".warp.y", warpLoc.getBlockY());
			config.getConfig().set(name + ".warp.z", warpLoc.getBlockZ());
			config.getConfig().set(name + ".warp.pitch", (double) warpLoc.getPitch());
			config.getConfig().set(name + ".warp.yaw", (double) warpLoc.getYaw());
			config.getConfig().set(name + ".warp.world", warpLoc.getWorld().getUID().toString());
		}
		
		try {
			config.saveConfig();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

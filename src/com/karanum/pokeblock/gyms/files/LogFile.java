package com.karanum.pokeblock.gyms.files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

public class LogFile {
	
	File file;
	
	public LogFile(File dir, String filename) throws IOException {
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		file = new File(dir, filename);
		if (!file.exists()) {
			file.createNewFile();
		}
	}
	
	public void writeLine(String s) {
		String timestamp = "[" + new Date().toString() + "] ";
		try {
			FileWriter writer = new FileWriter(file, true);
			writer.write(timestamp + s);
			writer.write("\n");
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeLines(Collection<String> s) {
		String timestamp = "[" + new Date().toString() + "] ";
		try {
			FileWriter writer = new FileWriter(file, true);
			for (String line : s) {
				writer.write(timestamp + line);
				writer.write("\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

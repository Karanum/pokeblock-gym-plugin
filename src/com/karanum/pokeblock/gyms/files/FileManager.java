package com.karanum.pokeblock.gyms.files;

import java.io.File;
import java.io.IOException;

import com.karanum.pokeblock.gyms.PokeBlockGyms;

public class FileManager {

	private File dataFolder;
	
	public FileManager() {
		dataFolder = PokeBlockGyms.instance.getDataFolder();
	}
	
	public ConfigFile loadConfig(String filename) {
		return createConfigFile(dataFolder, filename);
	}
	
	public ConfigFile loadConfig(String folder, String filename) {
		File configFolder = new File(dataFolder, folder);
		return createConfigFile(configFolder, filename);
	}
	
	public ConfigFile loadConfig(String folder, String filename, String template) {
		File configFolder = new File(dataFolder, folder);
		return createConfigFile(configFolder, filename, template);
	}
	
	public ConfigFile loadConfigIfExists(String filename) {
		File configFile = new File(dataFolder, filename);
		if (!configFile.exists()) {
			return null;
		}
		return createConfigFile(dataFolder, filename);
	}
	
	public LogFile getLogFile() {
		return createLogFile(dataFolder, "log.txt");
	}
	
	private ConfigFile createConfigFile(File dir, String filename) {
		try {
			return new ConfigFile(dir, filename);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private ConfigFile createConfigFile(File dir, String filename, String template) {
		try {
			return new ConfigFile(dir, filename, template);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private LogFile createLogFile(File dir, String filename) {
		try {
			return new LogFile(dir, filename);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}

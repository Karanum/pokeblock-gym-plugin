package com.karanum.pokeblock.gyms.files;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.bukkit.configuration.file.YamlConfiguration;

import com.karanum.pokeblock.gyms.PokeBlockGyms;

public class ConfigFile {

	File file;
	YamlConfiguration config;
	
	public ConfigFile(File dir, String filename) throws IOException {
		this(dir, filename, filename);
	}
	
	public ConfigFile(File dir, String filename, String defaultFile) throws IOException {
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		file = new File(dir, filename);
		if (!file.exists()) {
			loadDefaultConfig(defaultFile);
		}
		config = YamlConfiguration.loadConfiguration(file);
	}
	
	public YamlConfiguration getConfig() {
		return config;
	}
	
	public void saveConfig() throws IOException {
		config.save(file);
	}
	
	private void loadDefaultConfig(String name) throws IOException {
		InputStreamReader reader =
				new InputStreamReader(PokeBlockGyms.instance.getResource(name));
		config = YamlConfiguration.loadConfiguration(reader);
		config.options().copyDefaults(true);
		config.options().copyHeader(true);
		saveConfig();
	}
	
}

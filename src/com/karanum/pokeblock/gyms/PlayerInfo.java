package com.karanum.pokeblock.gyms;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

import com.karanum.pokeblock.gyms.files.ConfigFile;
import com.karanum.pokeblock.gyms.files.FileManager;

public class PlayerInfo {

	ConfigFile config;
	
	public PlayerInfo(UUID id) {
		FileManager manager = PokeBlockGyms.instance.getFileManager().get();
		config = manager.loadConfig("players", id.toString() + ".yml", "player.yml");
		
		GymManager gymMan = PokeBlockGyms.instance.getGymManager().orElse(null);
		if (gymMan == null) { 
			PokeBlockGyms.instance.getLogger().log(Level.SEVERE, 
					"Tried to load player data while plugin is reloading!");
			return;
		}
		
		for (String g : gymMan.getGymNames()) {
			if (!config.getConfig().contains("gyms." + g)) {
				config.getConfig().set("gyms." + g + ".cleared", false);
				config.getConfig().set("gyms." + g + ".cooldown", 0);
			}
		}
		saveConfig();
	}
	
	public boolean clearedGym(Gym g) {
		String gName = g.getName();
		return config.getConfig().getBoolean("gyms." + gName + ".cleared");
	}
	
	public void setClearedGym(Gym g) {
		config.getConfig().set("gyms." + g.getName() + ".cleared", true);
		saveConfig();
	}
	
	public void resetProgress() {
		GymManager gymMan = PokeBlockGyms.instance.getGymManager().get();
		for (String g : gymMan.getGymNames()) {
			config.getConfig().set("gyms." + g + ".cleared", false);
			config.getConfig().set("gyms." + g + ".cooldown", 0);
		}
		saveConfig();
	}
	
	public void resetGym(String gym) {
		if (config.getConfig().contains("gyms." + gym)) {
			config.getConfig().set("gyms." + gym + ".cleared", false);
			config.getConfig().set("gyms." + gym + ".cooldown", 0);
		}
		saveConfig();
	}
	
	public void saveConfig() {
		try {
			config.saveConfig();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public long getCooldown(String gym) {
		if (config.getConfig().contains("gyms." + gym)) {
			return config.getConfig().getLong("gyms." + gym + ".cooldown");
		}
		return 0;
	}
	
	public void setCooldown(String gym) {
		config.getConfig().set("gyms." + gym + ".cooldown", (new Date()).getTime());
		saveConfig();
	}
	
	public void logWin(String gym) {
		if (!config.getConfig().contains("gyms." + gym + ".leader")) {
			config.getConfig().set("gyms." + gym + ".leader.wins", 0);
			config.getConfig().set("gyms." + gym + ".leader.losses", 0);
		}
		
		String nodePath = "gyms." + gym + ".leader.wins";
		config.getConfig().set(nodePath, config.getConfig().getInt(nodePath) + 1);
		saveConfig();
	}
	
	public void logLoss(String gym) {
		if (!config.getConfig().contains("gyms." + gym + ".leader")) {
			config.getConfig().set("gyms." + gym + ".leader.wins", 0);
			config.getConfig().set("gyms." + gym + ".leader.losses", 0);
		}
		
		String nodePath = "gyms." + gym + ".leader.losses";
		config.getConfig().set(nodePath, config.getConfig().getInt(nodePath) + 1);
		saveConfig();
	}
	
	public HashMap<String, int[]> getLeaderStats() {
		HashMap<String, int[]> res = new HashMap<String, int[]>();
		for (String gym : config.getConfig().getConfigurationSection("gyms").getKeys(false)) {
			if (config.getConfig().contains("gyms." + gym + ".leader")) {
				int wins = config.getConfig().getInt("gyms." + gym + ".leader.wins");
				int losses = config.getConfig().getInt("gyms." + gym + ".leader.losses");
				res.put(gym, new int[] {wins, losses});
			}
		}
		return res;
	}
	
}

package com.karanum.pokeblock.gyms.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.GymManager;
import com.karanum.pokeblock.gyms.PlayerManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.files.LogFile;
import com.karanum.pokeblock.gyms.utils.FuzzyMatching;
import com.karanum.pokeblock.gyms.utils.Vocab;

public abstract class PluginCommand {
	
	public boolean execute(CommandSender sender, String[] args) {
		return false;
	}

	public abstract String getDescription();
	public abstract String getUsage();
	
	protected void showUsage(CommandSender sender) {
		sender.sendMessage(Vocab.MESSAGE_ERROR + "Invalid parameters! Usage: /" + getUsage());
	}
	
	protected boolean checkArguments(CommandSender sender, String[] args, int amount) {
		if (args.length != amount) {
			showUsage(sender);
			return false;
		}
		return true;
	}
	
	protected boolean checkArguments(CommandSender sender, String[] args, int min, int max) {
		if (args.length < min || args.length > max) {
			showUsage(sender);
			return false;
		}
		return true;
	}
	
	protected boolean checkPlayerSender(CommandSender sender) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This command can only be used by players!");
			return false;
		}
		return true;
	}
	
	protected boolean checkGymLeader(Player p, Gym g) {
		if (!g.isLeader(p)) {
			p.sendMessage(Vocab.MESSAGE_ERROR + "You may only use this command on your own gyms");
			return false;
		}
		return true;
	}
	
	protected boolean checkOnlinePlayer(CommandSender sender, OfflinePlayer p) {
		if (!(p instanceof Player)) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This can only be used on online players");
			return false;
		}
		return true;
	}
	
	protected Gym getGym(CommandSender sender, String name) {
		GymManager gymMan = PokeBlockGyms.instance.getGymManager().get();
		Gym g = gymMan.getGym(name);
		if (g != null) {
			return g;
		}
		
		sender.sendMessage(Vocab.MESSAGE_ERROR + "Unknown gym name: " + name);
		if (!PokeBlockGyms.instance.getConfigManager().get().getFuzzyMatching()) {
			return null;
		}
		
		String nearest = FuzzyMatching.getNearest(name, gymMan.getGymNames(), FuzzyMatching.getGymThreshold());
		if (nearest != null) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "Nearest possible match: " + nearest);
		}
		
		return null;
	}
	
	protected OfflinePlayer getPlayer(CommandSender sender, String name) {
		PlayerManager playerMan = PokeBlockGyms.instance.getPlayerManager().get();
		UUID id = playerMan.getPlayerId(name);
		if (id != null) {
			return Bukkit.getServer().getOfflinePlayer(id);
		}
		
		sender.sendMessage(Vocab.MESSAGE_ERROR + "Unknown player: " + name);
		if (!PokeBlockGyms.instance.getConfigManager().get().getFuzzyMatching()) {
			return null;
		}
		
		String nearest = FuzzyMatching.getNearest(name, playerMan.getAllKnownNames(), FuzzyMatching.getPlayerThreshold());
		if (nearest != null) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "Nearest possible match: " + nearest);
		}
		
		return null;
	}
	
	protected LogFile getLog() {
		return PokeBlockGyms.instance.getLogFile();
	}
	
}

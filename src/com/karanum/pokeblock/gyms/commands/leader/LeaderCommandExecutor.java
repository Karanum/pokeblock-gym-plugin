package com.karanum.pokeblock.gyms.commands.leader;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.GymManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.commands.PluginCommandExecutor;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class LeaderCommandExecutor extends PluginCommandExecutor {

	private final String helpHeader = "Leader commands:";
	
	public LeaderCommandExecutor() {
		super();
		addCommand(new CommandEndBattle(), "endbattle", "eb");
		addCommand(new CommandResult(), "result", "r");
		addCommand(new CommandWarp(), "warp");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "Only players can use this command");
			return true;
		}
		
		GymManager gymMan = PokeBlockGyms.instance.getGymManager().get();
		if (!sender.hasPermission("pbgyms.player") || !gymMan.isLeader((Player) sender)) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (args.length == 0) {
			showHelp(sender, helpHeader);
			return true;
		}
		
		String[] subargs = new String[args.length - 1];
		System.arraycopy(args, 1, subargs, 0, subargs.length);
		
		if (args[0].toLowerCase().equals("help") || args[0].equals("?")) {
			showHelp(sender, helpHeader);
			return true;
		}
		
		PluginCommand command = getCommand(args[0].toLowerCase());
		if (command != null) {
			return command.execute(sender, subargs);
		}

		showHelp(sender, helpHeader);
		return true;
	}
	
}

package com.karanum.pokeblock.gyms.commands.leader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.PlayerInfo;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Rewards;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandResult extends PluginCommand {

	private List<String> wins;
	private List<String> losses;
	
	public CommandResult() {
		wins = new ArrayList<String>();
		wins = Arrays.asList(new String[] {"win", "won", "yes", "y", "1", "true", "t", "w", "pear", "success", "gz", "gj"});
		
		losses = new ArrayList<String>();
		losses = Arrays.asList(new String[] {"loss", "lost", "lose", "no", "n", "0", "false", "flase", "f", "l", "apple", "failure", "joe"});
	}
	
	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!checkArguments(sender, args, 3) || !checkPlayerSender(sender)) { return true; }
		
		Gym g = getGym(sender, args[0]);
		if (g == null || !checkGymLeader((Player) sender, g)) { return true; }
		
		OfflinePlayer p = getPlayer(sender, args[1]);
		if (p == null || !checkOnlinePlayer(sender, p)) { return true; }
		
		PlayerInfo pinfo = PokeBlockGyms.instance.getPlayerManager().get().getPlayer((Player)p);
		boolean canRechallenge = PokeBlockGyms.instance.getConfigManager().get().getGymsRechallenge();
		if (pinfo.clearedGym(g) && !canRechallenge) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This player has already beaten the gym");
			return true;
		}
		
		long cooldown = pinfo.getCooldown(g.getName());
		long minCooldown = (new Date()).getTime() - PokeBlockGyms.instance.getConfigManager().get().getGymsCooldown();
		if (cooldown >= minCooldown) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This player is still on cooldown for this gym");
			return true;
		}
		
		String result = args[2];
		if (wins.contains(result)) {
			handleWin(sender, (Player)p, g, canRechallenge);
		} else if (losses.contains(result)) {
			handleLoss(sender, (Player)p, g);
		} else {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "Please enter either \"win\" or \"loss\"");
		}
		
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gl result <gym> <player> <win|loss>";
	}
	
	
	private void handleWin(CommandSender sender, Player p, Gym g, boolean canRechallenge) {
		PlayerInfo pinfo = PokeBlockGyms.instance.getPlayerManager().get().getPlayer(p);
		Rewards.giveGymRewards(sender, p, g, !pinfo.clearedGym(g));
		pinfo.setClearedGym(g);
		if (canRechallenge) {
			pinfo.setCooldown(g.getName());
		}
		
		p.sendMessage(Vocab.MESSAGE_SUCCESS + "You defeated " + ((Player) sender).getName() + " and cleared the " + g.getDisplayName() + "!");
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + "Marked a win for " + p.getName());
		getLog().writeLine(((Player) sender).getName() + " logged a WIN for " + p.getName());
		
		PokeBlockGyms.instance.getPlayerManager().get().getPlayer((Player) sender).logLoss(g.getName());
	}
	
	private void handleLoss(CommandSender sender, Player p, Gym g) {
		PokeBlockGyms.instance.getPlayerManager().get().getPlayer(p).setCooldown(g.getName());
		p.sendMessage(Vocab.MESSAGE_ERROR + "You have been defeated by " + ((Player) sender).getName() + " at the " + g.getDisplayName());
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + "Marked a loss for " + p.getName());
		getLog().writeLine(((Player) sender).getName() + " logged a LOSS for " + p.getName());
		
		PokeBlockGyms.instance.getPlayerManager().get().getPlayer((Player) sender).logWin(g.getName());
	}

	@Override
	public String getDescription() {
		return "Log a battle result";
	}
	
}

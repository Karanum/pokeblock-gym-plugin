package com.karanum.pokeblock.gyms.commands.leader;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.commands.PluginCommand;

public class CommandEndBattle extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!checkPlayerSender(sender)) { return true; }
		Player p = (Player) sender;
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "endbattle " + p.getName());
		
		getLog().writeLine(p.getName() + " used /endbattle");
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gl endbattle";
	}

	@Override
	public String getDescription() {
		return "Forcibly ends a gym battle";
	}
	
}

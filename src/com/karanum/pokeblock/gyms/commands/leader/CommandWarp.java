package com.karanum.pokeblock.gyms.commands.leader;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandWarp extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!checkPlayerSender(sender) || !checkArguments(sender, args, 1)) { return true; }
		Player p = (Player) sender;
		
		Gym g = getGym(sender, args[0]);
		if (g == null || !checkGymLeader(p, g)) { return true; }
		
		Location loc = g.getWarpLocation();
		if (loc == null) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This gym does not have a warp");
			return true;
		}
		
		p.teleport(loc, TeleportCause.COMMAND);
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + "Teleported to the " + g.getDisplayName());
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gl warp <gym>";
	}

	@Override
	public String getDescription() {
		return "Warp to your gym";
	}
	
}

package com.karanum.pokeblock.gyms.commands.debug;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.commands.PluginCommandExecutor;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class DebugCommandExecutor extends PluginCommandExecutor {

	private Set<UUID> allowedUUIDs = new HashSet<UUID>();
	
	public DebugCommandExecutor() {
		super();
		addCommand(new CommandConfigNode(), "confignode", "node");
		
		allowedUUIDs.add(UUID.fromString("684d253f-c2a8-479b-a1ea-a886386cad6c")); // Karanum
		allowedUUIDs.add(UUID.fromString("56876acf-1921-4a23-ac49-5c14209083a3")); // Snootiful
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if ((sender instanceof Player && !allowedUUIDs.contains(((Player) sender).getUniqueId())
				|| sender instanceof BlockCommandSender)) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (args.length == 0) {
			return false;
		}
		
		String[] subargs = new String[args.length - 1];
		System.arraycopy(args, 1, subargs, 0, subargs.length);
		
		PluginCommand command = getCommand(args[0].toLowerCase());
		if (command != null) {
			return command.execute(sender, subargs);
		}
		
		return false;
	}

}

package com.karanum.pokeblock.gyms.commands.debug;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.files.ConfigFile;
import com.karanum.pokeblock.gyms.files.FileManager;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandConfigNode extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!checkArguments(sender, args, 2)) { return true; }
		
		if (args[0].indexOf("..") != -1) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "You are not allowed to move back on the directory tree!");
			return true;
		}
		
		FileManager manager = PokeBlockGyms.instance.getFileManager().get();
		ConfigFile config = manager.loadConfigIfExists(args[0]);
		if (config == null) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "That configuration file could not be opened!");
			return true;
		}
		YamlConfiguration cfg = config.getConfig();
		if (!cfg.contains(args[1])) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "That configuration node does not exist!");
			return true;
		}
		
		sender.sendMessage(Vocab.MESSAGE_TITLE + args[1] + " = " + Vocab.MESSAGE_DATA + cfg.get(args[1]));		
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gd node <file> <node>";
	}

	@Override
	public String getDescription() {
		return "";
	}
	
}

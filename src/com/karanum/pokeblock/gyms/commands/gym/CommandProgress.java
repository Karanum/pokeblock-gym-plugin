package com.karanum.pokeblock.gyms.commands.gym;

import java.util.Date;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.GymManager;
import com.karanum.pokeblock.gyms.PlayerInfo;
import com.karanum.pokeblock.gyms.PlayerManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandProgress extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!checkArguments(sender, args, 0, 1)) { return true; }
		
		GymManager gymManager = PokeBlockGyms.instance.getGymManager().get();
		PlayerManager manager = PokeBlockGyms.instance.getPlayerManager().get();
		PlayerInfo player = null;
		String name = null;
		if (args.length == 0) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(Vocab.MESSAGE_ERROR + "Only players can omit the argument!");
				return true;
			} else {
				player = manager.getPlayer((Player) sender);
				name = ((Player) sender).getName();
			}
		} else {
			if (!sender.hasPermission("pbgyms.player.checkothers") &&
					!(sender instanceof Player && gymManager.isLeader((Player) sender))) {
				sender.sendMessage(Vocab.NO_PERMISSIONS);
				return true;
			}
			OfflinePlayer p = getPlayer(sender, args[0]);
			if (p == null) { return true; }
			player = manager.getOfflinePlayer(p);
			name = p.getName();
		}		
		
		String message = Vocab.MESSAGE_TITLE + Vocab.DELIMITER + "\n";
		message += name + "'s Gym Progress\n";
		for (Gym gym : gymManager.getGymList()) {
			message += "- " + gym.getDisplayName() + ": ";
			message += (player.clearedGym(gym) ? Vocab.MESSAGE_SUCCESS + "Cleared" : Vocab.MESSAGE_ERROR + "Not cleared");
			if (hasCooldown(gym, player)) {
				message += Vocab.COLOR_COOLDOWN + " " + getCooldown(gym, player);
			}
			message += Vocab.MESSAGE_TITLE + "\n";
		}
		message += Vocab.MESSAGE_TITLE + Vocab.DELIMITER;
		
		sender.sendMessage(message);		
		return true;
	}
	
	private boolean hasCooldown(Gym g, PlayerInfo p) {
		long start = p.getCooldown(g.getName());
		if (start == 0) { return false; }
		
		long current = new Date().getTime();
		int cooldown = PokeBlockGyms.instance.getConfigManager().get().getGymsCooldown();
		return (current < start + cooldown);
	}
	
	private String getCooldown(Gym g, PlayerInfo p) {
		long start = p.getCooldown(g.getName());
		if (start == 0) { return null; }
		
		long current = new Date().getTime();
		int cooldown = PokeBlockGyms.instance.getConfigManager().get().getGymsCooldown();
		long diff = start + cooldown - current;
		
		long hours = diff / 3600000;
		long mins = (diff % 3600000) / 60000;
		return "[" + hours + "h " + mins + "m left]";
	}
	
	@Override
	public String getUsage() {
		return "gym check [player]";
	}

	@Override
	public String getDescription() {
		return "Check your progress and cooldowns";
	}
	
}

package com.karanum.pokeblock.gyms.commands.gym;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.commands.PluginCommandExecutor;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class PlayerCommandExecutor extends PluginCommandExecutor {
	
	private final String helpHeader = "Gym commands:";
	
	public PlayerCommandExecutor() {
		super();
		addCommand(new CommandProgress(), "check", "progress", "stats");
		addCommand(new CommandInfo(), "info");
		addCommand(new CommandRules(), "rules");
		addCommand(new CommandGymLeaders(), "leaders");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("pbgyms.player")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (args.length == 0) {
			showHelp(sender, helpHeader);
			return true;
		}
		
		String[] subargs = new String[args.length - 1];
		System.arraycopy(args, 1, subargs, 0, subargs.length);
		
		if (args[0].toLowerCase().equals("help") || args[0].equals("?")) {
			showHelp(sender, helpHeader);
			return true;
		}
		
		PluginCommand command = getCommand(args[0].toLowerCase());
		if (command != null) {
			return command.execute(sender, subargs);
		}

		showHelp(sender, helpHeader);
		return true;
	}
	
	@Override
	protected void showHelp(CommandSender sender, String header) {
		String message = getHelpMessage(header);
		String delim = Vocab.MESSAGE_TITLE + Vocab.DELIMITER;
		message = message.substring(0, message.length() - delim.length());
		
		if (sender instanceof Player && PokeBlockGyms.instance.getGymManager().get().isLeader((Player) sender)) {
			message += Vocab.MESSAGE_TITLE + "To check gym leader commands, use " + Vocab.MESSAGE_DATA + "/gl help\n";
		}
		if (sender.hasPermission("pbgyms.admin")) {
			message += Vocab.MESSAGE_TITLE + "To check admin commands, use " + Vocab.MESSAGE_DATA + "/ga help\n";
		}
		message += delim;
		sender.sendMessage(message);
	}

}

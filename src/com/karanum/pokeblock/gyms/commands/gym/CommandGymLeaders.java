package com.karanum.pokeblock.gyms.commands.gym;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.GymManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandGymLeaders extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		GymManager gymManager = PokeBlockGyms.instance.getGymManager().get();
		
		Set<UUID> leaders = new HashSet<UUID>();
		for (Gym gym : gymManager.getGymList()) {
			leaders.addAll(gym.getLeaders());
		}
		
		String message = Vocab.MESSAGE_SUCCESS + "Gym Leaders Online:" + Vocab.MESSAGE_DATA;
		for (UUID id : leaders) {
			Player leader = Bukkit.getServer().getPlayer(id);
			message += " " + leader.getDisplayName();
		}
		
		sender.sendMessage(message);		
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gym leaders";
	}

	@Override
	public String getDescription() {
		return "See all online gym leaders";
	}
	
}

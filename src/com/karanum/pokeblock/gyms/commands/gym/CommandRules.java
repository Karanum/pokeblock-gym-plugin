package com.karanum.pokeblock.gyms.commands.gym;

import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.files.ConfigFile;
import com.karanum.pokeblock.gyms.files.FileManager;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandRules extends PluginCommand {
	
	@Override
	public boolean execute(CommandSender sender, String[] args) {
		FileManager manager = PokeBlockGyms.instance.getFileManager().get();
		ConfigFile config = manager.loadConfig("gyms.yml");
		String message = "";
		
		message += Vocab.MESSAGE_TITLE + Vocab.DELIMITER + "\n";
		for (String line : config.getConfig().getStringList("rules")) {
			message += Vocab.MESSAGE_TITLE + "- " + Vocab.MESSAGE_DATA + line + "\n";
		}
		message += Vocab.MESSAGE_TITLE + Vocab.DELIMITER;
		
		sender.sendMessage(message);
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gym rules";
	}

	@Override
	public String getDescription() {
		return "View the general gym rules";
	}
	
}

package com.karanum.pokeblock.gyms.commands.gym;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.GymManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Rewards;
import com.karanum.pokeblock.gyms.utils.Vocab;

import net.milkbowl.vault.economy.Economy;

public class CommandInfo extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!checkArguments(sender, args, 0, 1)) { return true; }
		
		GymManager gymMan = PokeBlockGyms.instance.getGymManager().get();
		
		if (args.length == 0) {
			listGyms(sender, gymMan);
			return true;
		}
		
		Gym g = getGym(sender, args[0].toLowerCase());
		if (g == null) {
			return true;
		}
		
		String badgeName = getBadgeName(g);
		if (badgeName == null) { badgeName = "???"; }
		
		String color = Vocab.MESSAGE_TITLE;
		String message = color + Vocab.DELIMITER + "\n";
		message += g.getDisplayName() + " Info:\n";
		message += "  Badge: " + Vocab.MESSAGE_DATA + badgeName + color + "\n";
		message += "  Rules:\n" + Vocab.MESSAGE_DATA;
		for (String s : g.getRules()) {
			message += "  - " + s + "\n";
		}
		message += color;
		message += "  Rewards:\n" + Vocab.MESSAGE_DATA;
		for (String s : makeRewardList(g)) {
			message += "  - " + s + "\n";
		}
		message += color;
		message += "  Online leaders:";
		for (UUID uid : g.getLeaders()) {
			OfflinePlayer p = Bukkit.getServer().getOfflinePlayer(uid);
			if (p.isOnline()) {
				message += Vocab.MESSAGE_PLAYER_ONLINE;
			} else {
				message += Vocab.MESSAGE_PLAYER_OFFLINE;
			}
			message += " " + p.getName();
		}
		message += color + "\n" + Vocab.DELIMITER;
		
		sender.sendMessage(message);
		return true;
	}
	
	@Override
	public String getUsage() {
		return "gym info [gym]";
	}
	
	private void listGyms(CommandSender sender, GymManager gymMan) {
		Collection<String> gyms = gymMan.getGymNames();
		if (gyms.isEmpty()) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "There are no gyms defined");
			return;
		}
		
		Iterator<String> iter = gyms.iterator();
		String result = Vocab.MESSAGE_SUCCESS + "Available gyms: " + iter.next();
		while (iter.hasNext()) {
			result += ", " + iter.next();
		}
		sender.sendMessage(result);
	}
	
	private String getBadgeName(Gym g) {
		String badgeId = g.getBadge();
		int damage = 0;
		
		String[] args = badgeId.split(" ");
		if (args.length == 0) {
			return null;
		}
		if (args.length >= 2) {
			damage = Integer.parseInt(args[1]);
		}
		
		ItemStack badge = Rewards.getItemStack(args[0], 1, (short) damage);
		return badge.getType().name();
	}
	
	private List<String> makeRewardList(Gym g) {
		List<String> rewards = new ArrayList<String>();
		Economy econ = PokeBlockGyms.instance.getEconomy().orElse(null);
		if (econ != null && g.getRewardMoney() > 0) {
			rewards.add(econ.format(g.getRewardMoney()));
		}
		
		for (String item : g.getRewardItems()) {
			ItemStack stack = getItemStack(item);
			if (stack == null) { continue; }
			String name = stack.getType().toString();
			String amount = "x" + stack.getAmount();
			rewards.add(name + " " + amount);
		}
		
		for (String line : g.getRewardText()) {
			rewards.add(line);
		}
		
		return rewards;
	}
	
	private ItemStack getItemStack(String item) {
		String[] args = item.split(" ");
		int amount = 1;
		int damage = 0;
		
		if (args.length == 0) {
			return null; 
		}
		if (args.length == 2) {
			amount = Integer.parseInt(args[1]);
		} else if (args.length >= 3) {
			damage = Integer.parseInt(args[1]);
			amount = Integer.parseInt(args[2]);
		}
		
		ItemStack stack = Rewards.getItemStack(args[0], amount, (short) damage);
		if (stack.getType() == Material.AIR) {
			return null;
		}
		return stack;
	}

	@Override
	public String getDescription() {
		return "View info on a gym";
	}
	
}

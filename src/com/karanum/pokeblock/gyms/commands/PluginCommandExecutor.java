package com.karanum.pokeblock.gyms.commands;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.utils.Vocab;

public abstract class PluginCommandExecutor implements CommandExecutor {

	private HashMap<String, PluginCommand> commandMap;
	private ArrayList<PluginCommand> commandList;
	
	public PluginCommandExecutor() {
		commandMap = new HashMap<String, PluginCommand>();
		commandList = new ArrayList<PluginCommand>();
	}
	
	protected void addCommand(PluginCommand command, String... names) {
		commandList.add(command);
		for (String name : names) {
			commandMap.put(name, command);
		}
	}
	
	protected PluginCommand getCommand(String name) {
		return commandMap.get(name);
	}
	
	protected ArrayList<PluginCommand> getAllCommands() {
		return commandList;
	}
	
	protected void showHelp(CommandSender sender, String header) {
		sender.sendMessage(getHelpMessage(header));
	}
	
	protected String getHelpMessage(String header) {
		String message = Vocab.MESSAGE_TITLE + Vocab.DELIMITER + "\n";
		message += header + "\n";
		
		for (PluginCommand command : getAllCommands()) {
			message += Vocab.MESSAGE_DATA + "/" + command.getUsage() + Vocab.MESSAGE_TITLE + 
						" - " + command.getDescription() + "\n";
		}
		
		message += Vocab.MESSAGE_TITLE + "\n<> = Required, [] = Optional\n";
		message += Vocab.MESSAGE_TITLE + Vocab.DELIMITER;
		return message;
	}
	
}

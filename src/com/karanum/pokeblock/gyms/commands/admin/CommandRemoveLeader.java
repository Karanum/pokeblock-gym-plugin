package com.karanum.pokeblock.gyms.commands.admin;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandRemoveLeader extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!sender.hasPermission("pbgyms.admin.leaders")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (!checkArguments(sender, args, 2)) { return true; }
		
		Gym g = getGym(sender, args[0]);
		if (g == null) { return true; }
		
		OfflinePlayer p = getPlayer(sender, args[1]);
		if (p == null) { return true; }
		
		if (!g.isLeader(p)) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This player is not a leader of this gym");
			return true;
		}
		
		g.removeLeader(p);
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + p.getName() + " has been removed as a gym leader");
		getLog().writeLine(sender.getName() + " removed " + p.getName() + " from the " + g.getDisplayName());
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga removeleader <gym> <player>";
	}

	@Override
	public String getDescription() {
		return "Remove a leader from a gym";
	}
	
}

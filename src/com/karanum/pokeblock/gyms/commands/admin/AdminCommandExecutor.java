package com.karanum.pokeblock.gyms.commands.admin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.commands.PluginCommandExecutor;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class AdminCommandExecutor extends PluginCommandExecutor {

	private final String helpHeader = "Admin commands:";
	
	public AdminCommandExecutor() {
		super();
		addCommand(new CommandAddLeader(), "addleader", "addl");
		addCommand(new CommandBadge(), "badge");
		addCommand(new CommandLog(), "log", "stats", "audit");
		addCommand(new CommandReload(), "reload");
		addCommand(new CommandRemoveLeader(), "removeleader", "removel");
		addCommand(new CommandReset(), "reset");
		addCommand(new CommandSetWarp(), "setwarp");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("pbgyms.admin")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (args.length == 0) {
			showHelp(sender, helpHeader);
			return true;
		}
		
		String[] subargs = new String[args.length - 1];
		System.arraycopy(args, 1, subargs, 0, subargs.length);
		
		if (args[0].toLowerCase().equals("help") || args[0].equals("?")) {
			showHelp(sender, helpHeader);
			return true;
		}
		
		PluginCommand command = getCommand(args[0].toLowerCase());
		if (command != null) {
			return command.execute(sender, subargs);
		}

		showHelp(sender, helpHeader);
		return true;
	}

}

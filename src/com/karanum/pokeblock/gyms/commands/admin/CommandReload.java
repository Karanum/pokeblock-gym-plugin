package com.karanum.pokeblock.gyms.commands.admin;

import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandReload extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!sender.hasPermission("pbgyms.admin.reload")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		PokeBlockGyms.instance.reload();
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + "PokeBlockGyms has been reloaded successfully");
		getLog().writeLine(sender.getName() + " reloaded the plugin");
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga reload";
	}

	@Override
	public String getDescription() {
		return "Reload the gym plugin";
	}
	
}

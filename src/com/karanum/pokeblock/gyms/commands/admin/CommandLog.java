package com.karanum.pokeblock.gyms.commands.admin;

import java.util.HashMap;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.GymManager;
import com.karanum.pokeblock.gyms.PlayerInfo;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandLog extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!sender.hasPermission("pbgyms.admin.log")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (!checkArguments(sender, args, 1)) { return true; }
		
		OfflinePlayer p = getPlayer(sender, args[0]);
		if (p == null) { return true; }
		
		GymManager gymMan = PokeBlockGyms.instance.getGymManager().get();
		PlayerInfo player = PokeBlockGyms.instance.getPlayerManager().get().getOfflinePlayer(p);
		HashMap<String, int[]> stats = player.getLeaderStats();
		String message = Vocab.MESSAGE_TITLE + Vocab.DELIMITER;
		
		if (stats.keySet().isEmpty()) {
			message += Vocab.MESSAGE_DATA + "\nThis player has no known leader stats";
		} else {
			for (String gym : stats.keySet()) {
				Gym g = gymMan.getGym(gym);
				if (g == null) { continue; }
				
				String name = g.getDisplayName();
				message += "\n" + Vocab.MESSAGE_DATA + name + ": ";
				message += Vocab.MESSAGE_SUCCESS + stats.get(gym)[0] + " wins ";
				message += Vocab.MESSAGE_DATA + "/ ";
				message += Vocab.MESSAGE_ERROR + stats.get(gym)[1] + " losses";
	 		}
		}
		
		message += "\n" + Vocab.MESSAGE_TITLE + Vocab.DELIMITER;
		sender.sendMessage(message);
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga stats <player>";
	}

	@Override
	public String getDescription() {
		return "View a leader's win/loss stats";
	}
	
}

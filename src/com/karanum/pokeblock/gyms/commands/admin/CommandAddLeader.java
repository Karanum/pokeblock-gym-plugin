package com.karanum.pokeblock.gyms.commands.admin;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandAddLeader extends PluginCommand {
	
	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!sender.hasPermission("pbgyms.admin.leaders")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (!checkArguments(sender, args, 2)) { return true; }
		
		Gym g = getGym(sender, args[0]);
		if (g == null) { return true; }
		
		OfflinePlayer p = getPlayer(sender, args[1]);
		if (p == null) { return true; }
		
		if (g.isLeader(p)) {
			sender.sendMessage(Vocab.MESSAGE_ERROR + "This player is already a leader of this gym");
			return true;
		}
		
		g.addLeader(p);
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + p.getName() + " has been added as a gym leader");
		getLog().writeLine(sender.getName() + " added " + p.getName() + " to the " + g.getDisplayName());
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga addleader <gym> <player>";
	}

	@Override
	public String getDescription() {
		return "Add a leader to a gym";
	}
	
}

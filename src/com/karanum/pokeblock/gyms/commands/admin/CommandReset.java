package com.karanum.pokeblock.gyms.commands.admin;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.PlayerInfo;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandReset extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!sender.hasPermission("pbgyms.admin.reset")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (!checkArguments(sender, args, 1, 2)) { return true; }
		
		OfflinePlayer p = getPlayer(sender, args[0]);
		if (p == null) { return true; }
		
		PlayerInfo pInfo = PokeBlockGyms.instance.getPlayerManager().get().getOfflinePlayer(p);
		
		if (args.length == 2) {
			Gym g = getGym(sender, args[1]);
			if (g == null) { return true; }

			pInfo.resetGym(g.getName());
			sender.sendMessage(Vocab.MESSAGE_SUCCESS + g.getDisplayName() + " progress for " + p.getName() + " has been reset");
			getLog().writeLine(p.getName() + " reset " + g.getDisplayName() + " progress for " + p.getName());
		} else {
			pInfo.resetProgress();
			sender.sendMessage(Vocab.MESSAGE_SUCCESS + "All gym progress for " + p.getName() + " has been reset");
			getLog().writeLine(p.getName() + " reset all gym progress for " + p.getName());
		}
		
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga reset <player> [gym]";
	}

	@Override
	public String getDescription() {
		return "Reset a player's progress";
	}
	
}

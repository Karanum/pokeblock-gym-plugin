package com.karanum.pokeblock.gyms.commands.admin;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandSetWarp extends PluginCommand {

	@Override
	public boolean execute(CommandSender sender, String[] args) {	
		if (!sender.hasPermission("pbgyms.admin.setwarp")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (!checkPlayerSender(sender) || !checkArguments(sender, args, 1)) { return true; }
		Player p = (Player) sender;
		
		Gym g = getGym(sender, args[0]);
		if (g == null) { return true; }
		
		g.setWarpLocation(p.getLocation());
		sender.sendMessage(Vocab.MESSAGE_SUCCESS + "Warp location for the " + g.getDisplayName() + " has been updated");
		
		getLog().writeLine(p.getName() + " moved the warp of the " + g.getDisplayName());
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga setwarp <gym>";
	}

	@Override
	public String getDescription() {
		return "Set a gym's warp point";
	}
	
}

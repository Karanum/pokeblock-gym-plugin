package com.karanum.pokeblock.gyms.commands.admin;

import java.util.ArrayList;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.PlayerInfo;
import com.karanum.pokeblock.gyms.PokeBlockGyms;
import com.karanum.pokeblock.gyms.commands.PluginCommand;
import com.karanum.pokeblock.gyms.utils.Rewards;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class CommandBadge extends PluginCommand {

	private ArrayList<String> confirms;
	
	public CommandBadge() {
		confirms = new ArrayList<String>();
		confirms.add("yes");
		confirms.add("true");
		confirms.add("force");
		confirms.add("y");
		confirms.add("t");
	}
	
	@Override
	public boolean execute(CommandSender sender, String[] args) {
		if (!sender.hasPermission("pbgyms.admin.badge")) {
			sender.sendMessage(Vocab.NO_PERMISSIONS);
			return true;
		}
		
		if (!checkArguments(sender, args, 2, 3)) { return true; }
		
		Gym g = getGym(sender, args[0]);
		if (g == null) { return true; }
		
		OfflinePlayer p = getPlayer(sender, args[1]);
		if (p == null || !checkOnlinePlayer(sender, p)) { return true; }
		
		if (!(args.length == 3 && confirms.contains(args[2]))) {
			PlayerInfo pInfo = PokeBlockGyms.instance.getPlayerManager().get().getOfflinePlayer(p);
			if (!pInfo.clearedGym(g)) {
				sender.sendMessage(Vocab.MESSAGE_ERROR + "This player has not cleared the gym yet");
				return true;
			}
		}
		
		String badge = g.getBadge();
		Rewards.giveBadge(sender, (Player) p, badge);
		getLog().writeLine(sender.getName() + " rewarded the " + g.getDisplayName() + " badge to " + p.getName());
		return true;
	}
	
	@Override
	public String getUsage() {
		return "ga badge <gym> <player> [force?]";
	}

	@Override
	public String getDescription() {
		return "Give a player a lost gym badge";
	}
	
}

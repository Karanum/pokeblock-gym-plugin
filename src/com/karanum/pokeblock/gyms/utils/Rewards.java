package com.karanum.pokeblock.gyms.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.karanum.pokeblock.gyms.Gym;
import com.karanum.pokeblock.gyms.PokeBlockGyms;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.minecraft.server.v1_7_R3.Item;

public abstract class Rewards {

	public static void giveGymRewards(CommandSender sender, Player p, Gym g, boolean includeBadge) {
		if (includeBadge) {
			giveBadge(sender, p, g.getBadge());
		}
		
		int money = g.getRewardMoney();
		if (money > 0) {
			giveMoney(sender, p, money);
		}
		
		List<String> items = g.getRewardItems();
		if (!items.isEmpty()) {
			giveItems(sender, p, items);
		}
		
		List<String> commands = g.getRewardCommands();
		if (!commands.isEmpty()) {
			CommandSender console = Bukkit.getConsoleSender();
			for (String cmd : commands) {
				Bukkit.dispatchCommand(console, cmd);
			}
		}
		
		List<String> text = g.getRewardText();
		for (String line : text) {
			p.sendMessage(Vocab.MESSAGE_SUCCESS + line);
		}
	}
	
	public static void giveMoney(CommandSender sender, Player p, double amount) {
		Economy econ = PokeBlockGyms.instance.getEconomy().orElse(null);
		if (econ == null) {
			return;
		}
		EconomyResponse res = econ.depositPlayer(p, amount);
		if (res.transactionSuccess()) {
			p.sendMessage(Vocab.MESSAGE_SUCCESS + "You received " + econ.format(res.amount));
		} else {
			showEconError(sender, res);
		}
	}
	
	public static void giveItems(CommandSender sender, Player p, Collection<String> items) {
		boolean overflow = false;
		for (String item : items) {
			overflow = giveItem(sender, p, item) || overflow;
		}
		if (overflow) {
			p.sendMessage(Vocab.MESSAGE_ERROR + "Your inventory is full! Excess items have been dropped!");
		}
	}
	
	public static void giveBadge(CommandSender sender, Player p, String badge) {
		String[] args = badge.split(" ");
		int damage = 0;
		
		if (args.length == 0) {
			showFormatError(sender, badge);
			return;
		}
		if (args.length >= 2) {
			damage = Integer.parseInt(args[1]);
		}
		
		ItemStack stack = getItemStack(args[0], 1, (short) damage);
		if (stack == null || stack.getType() == Material.AIR) {
			showItemError(sender, args[0]);
			return;
		}
		
		ItemMeta meta = stack.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(p.getName());
		lore.add(p.getUniqueId().toString());
		meta.setLore(lore);
		stack.setItemMeta(meta);
		
		HashMap<Integer, ItemStack> items = p.getInventory().addItem(stack);
		if (!items.isEmpty()) {
			p.getWorld().dropItemNaturally(p.getLocation(), items.get(0));
			p.sendMessage(Vocab.MESSAGE_ERROR + "Your inventory is full! Your badge has been dropped!");
		}
	}
	
	private static boolean giveItem(CommandSender sender, Player p, String item) {
		String[] args = item.split(" ");
		int amount = 1;
		int damage = 0;
		
		if (args.length == 0) { 
			showFormatError(sender, item);
			return false; 
		}
		if (args.length == 2) {
			amount = Integer.parseInt(args[1]);
		} else if (args.length >= 3) {
			damage = Integer.parseInt(args[1]);
			amount = Integer.parseInt(args[2]);
		}
		
		ItemStack stack = getItemStack(args[0], amount, (short) damage);
		if (stack == null || stack.getType() == Material.AIR) {
			showItemError(sender, args[0]);
			return false;
		}
		
		HashMap<Integer, ItemStack> items = p.getInventory().addItem(stack);
		if (!items.isEmpty()) {
			p.getWorld().dropItemNaturally(p.getLocation(), items.get(0));
			return true;
		}
		return false;
	}
	
	public static ItemStack getItemStack(String name, int amount, short damage) {
		ItemStack stack = CraftItemStack.asNewCraftStack((Item) Item.REGISTRY.get(name));
		stack.setAmount(amount);
		stack.setDurability(damage);
		return stack;
	}
	
	private static void showFormatError(CommandSender sender, String item) {
		sender.sendMessage(Vocab.MESSAGE_ERROR + "An error occurred while giving out the rewards");
		PokeBlockGyms.instance.getLogger().log(Level.SEVERE, "Incorrect reward format: " + item);
	}
	
	private static void showItemError(CommandSender sender, String item) {
		sender.sendMessage(Vocab.MESSAGE_ERROR + "An error occurred while giving out the rewards");
		PokeBlockGyms.instance.getLogger().log(Level.SEVERE, "Unknown item name: " + item);
	}
	
	private static void showEconError(CommandSender sender, EconomyResponse res) {
		sender.sendMessage(Vocab.MESSAGE_ERROR + "An error occurred while giving out the rewards");
		PokeBlockGyms.instance.getLogger().log(Level.SEVERE, "An economy error occurred: " + res.errorMessage);
	}
	
}

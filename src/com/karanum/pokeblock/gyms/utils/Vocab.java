package com.karanum.pokeblock.gyms.utils;

import org.bukkit.ChatColor;

import com.karanum.pokeblock.gyms.ConfigManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;

public abstract class Vocab {
	
	public static String DELIMITER = "================================";
	
	public static String MESSAGE_ERROR = ChatColor.RED.toString();
	public static String MESSAGE_SUCCESS = ChatColor.GREEN.toString();
	public static String MESSAGE_TITLE = ChatColor.DARK_AQUA.toString();
	public static String MESSAGE_DATA = ChatColor.WHITE.toString();
	public static String MESSAGE_PLAYER_ONLINE = ChatColor.WHITE.toString();
	public static String MESSAGE_PLAYER_OFFLINE = ChatColor.GRAY.toString();
	
	public static String COLOR_LEADER_JOIN = ChatColor.YELLOW.toString();
	public static String COLOR_COOLDOWN = ChatColor.GOLD.toString();
	
	public static String NO_PERMISSIONS = MESSAGE_ERROR + "You do not have permission to use this command!";
	
	public static void reload() {
		ConfigManager config = PokeBlockGyms.instance.getConfigManager().get();
		MESSAGE_ERROR = ChatColor.COLOR_CHAR + "" + config.getColor("error");
		MESSAGE_SUCCESS = ChatColor.COLOR_CHAR + "" + config.getColor("success");
		MESSAGE_TITLE = ChatColor.COLOR_CHAR + "" + config.getColor("title");
		MESSAGE_DATA = ChatColor.COLOR_CHAR + "" + config.getColor("info");
		MESSAGE_PLAYER_ONLINE = ChatColor.COLOR_CHAR + "" + config.getColor("online-player");
		MESSAGE_PLAYER_OFFLINE = ChatColor.COLOR_CHAR + "" + config.getColor("offline-player");
		COLOR_LEADER_JOIN = ChatColor.COLOR_CHAR + "" + config.getColor("leader-join");
		COLOR_COOLDOWN = ChatColor.COLOR_CHAR + "" + config.getColor("cooldown");
	}
	
}

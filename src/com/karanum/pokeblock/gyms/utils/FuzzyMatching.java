package com.karanum.pokeblock.gyms.utils;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import com.karanum.pokeblock.gyms.ConfigManager;
import com.karanum.pokeblock.gyms.PokeBlockGyms;

public abstract class FuzzyMatching {
	
	public static int getGymThreshold() {
		ConfigManager manager = PokeBlockGyms.instance.getConfigManager().get();
		return manager.getFuzzyGymThreshold();
	}
	
	public static int getPlayerThreshold() {
		ConfigManager manager = PokeBlockGyms.instance.getConfigManager().get();
		return manager.getFuzzyPlayerThreshold();
	}
	
	public static String getNearest(String input, Collection<String> options, int threshold) {
		ConfigManager manager = PokeBlockGyms.instance.getConfigManager().get();
		if (!manager.getFuzzyMatching()) {
			return null;
		}
		
		int lowestDistance = 0;
		String nearest = null;
		
		for (String option : options) {
			int distance = StringUtils.getLevenshteinDistance(input, option, threshold);
			if (distance != -1) {
				if (distance < lowestDistance || nearest == null) {
					lowestDistance = distance;
					nearest = option;
				}
			}
		}
		
		return nearest;
	}
	
}

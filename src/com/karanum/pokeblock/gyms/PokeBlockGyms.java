package com.karanum.pokeblock.gyms;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.karanum.pokeblock.gyms.commands.admin.AdminCommandExecutor;
import com.karanum.pokeblock.gyms.commands.debug.DebugCommandExecutor;
import com.karanum.pokeblock.gyms.commands.gym.PlayerCommandExecutor;
import com.karanum.pokeblock.gyms.commands.leader.LeaderCommandExecutor;
import com.karanum.pokeblock.gyms.files.FileManager;
import com.karanum.pokeblock.gyms.files.LogFile;
import com.karanum.pokeblock.gyms.utils.Vocab;

import net.milkbowl.vault.economy.Economy;

public class PokeBlockGyms extends JavaPlugin {

	public static PokeBlockGyms instance;
	public static Logger logger;
	
	private FileManager fileMan;
	private ConfigManager configMan;
	private GymManager gymMan;
	private PlayerManager playerMan;
	private Economy econ;
	
	private LogFile log;
	
	@Override
	public void onEnable() {
		instance = this;
		logger = getLogger();
		
		econ = null;
		fileMan = new FileManager();
		configMan = new ConfigManager();
		gymMan = new GymManager();
		playerMan = new PlayerManager();
		
		log = fileMan.getLogFile();
		Vocab.reload();
		
		getServer().getPluginManager().registerEvents(playerMan, this);
		
		getCommand("gym").setExecutor(new PlayerCommandExecutor());
		getCommand("leader").setExecutor(new LeaderCommandExecutor());
		getCommand("gymadmin").setExecutor(new AdminCommandExecutor());
		getCommand("gymdebug").setExecutor(new DebugCommandExecutor());
		
		logger.log(Level.INFO, "Gym plugin has loaded successfully!");
	}
	
	@Override
	public void onDisable() {
		logger = null;
		instance = null;
		
		gymMan = null;
		fileMan = null;
		playerMan = null;
		
		log = null;
		econ = null;
	}
	
	public void reload() {
		Bukkit.getServer().getPluginManager().disablePlugin(this);
		Bukkit.getServer().getPluginManager().enablePlugin(this);
	}
	
	public Optional<FileManager> getFileManager() {
		return Optional.ofNullable(fileMan);
	}
	
	public Optional<ConfigManager> getConfigManager() {
		return Optional.ofNullable(configMan);
	}
	
	public Optional<GymManager> getGymManager() {
		return Optional.ofNullable(gymMan);
	}
	
	public Optional<PlayerManager> getPlayerManager() {
		return Optional.ofNullable(playerMan);
	}
	
	public Optional<Economy> getEconomy() {
		if (econ == null) {
			if (!loadEconomy()) {
				logger.log(Level.WARNING, "Vault economy hook could not be instantiated!");
				logger.log(Level.WARNING, "Gyms cannot reward money");
			}
		}
		return Optional.ofNullable(econ);
	}
	
	public LogFile getLogFile() {
		return log;
	}
	
	private boolean loadEconomy() {		
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			getLogger().log(Level.WARNING, "Vault was not found!");
			return false;
		}
		RegisteredServiceProvider<Economy> econProvider = getServer().getServicesManager().getRegistration(Economy.class);
		if (econProvider == null) { return false; }
		econ = econProvider.getProvider();
		return econ != null;
	}
	
}

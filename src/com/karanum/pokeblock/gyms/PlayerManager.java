package com.karanum.pokeblock.gyms;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.karanum.pokeblock.gyms.files.ConfigFile;
import com.karanum.pokeblock.gyms.files.FileManager;
import com.karanum.pokeblock.gyms.utils.Vocab;

public class PlayerManager implements Listener {

	private HashMap<UUID, PlayerInfo> players;
	private ConfigFile playerList;
	
	public PlayerManager() {
		FileManager manager = PokeBlockGyms.instance.getFileManager().get();
		playerList = manager.loadConfig("players.yml");
		players = new HashMap<UUID, PlayerInfo>();
		
		for (Player p : PokeBlockGyms.instance.getServer().getOnlinePlayers()) {
			addPlayer(p);
		}
	}
	
	public void addPlayer(Player p) {
		if (!players.containsKey(p.getUniqueId())) {
			players.put(p.getUniqueId(), new PlayerInfo(p.getUniqueId()));
		}
		playerList.getConfig().set("players." + p.getName().toLowerCase(), p.getUniqueId().toString());
		saveConfig();
	}
	
	public void removePlayer(Player p) {
		players.remove(p.getUniqueId());
	}
	
	public PlayerInfo getPlayer(Player p) {
		return players.get(p.getUniqueId());
	}
	
	public PlayerInfo getOfflinePlayer(OfflinePlayer p) {
		PlayerInfo info = players.get(p.getUniqueId());
		if (info == null) {
			info = new PlayerInfo(p.getUniqueId());
		}
		return info;
	}
	
	public UUID getPlayerId(String name) {
		if (!playerList.getConfig().contains("players." + name.toLowerCase())) {
			return null;
		}
		String id = playerList.getConfig().getString("players." + name.toLowerCase());
		return UUID.fromString(id);
	}
	
	public Collection<String> getAllKnownNames() {
		return playerList.getConfig().getConfigurationSection("players").getKeys(false);
	}
	
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onPlayerJoin(PlayerJoinEvent e) {
		addPlayer(e.getPlayer());
		Collection<Gym> gyms = PokeBlockGyms.instance.getGymManager().get().leaderOf(e.getPlayer());
		if (!gyms.isEmpty()) {
			Bukkit.broadcastMessage(Vocab.COLOR_LEADER_JOIN + "A gym leader has joined the server!");
		}
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		removePlayer(e.getPlayer());
	}
	
	private void saveConfig() {
		try {
			playerList.saveConfig();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

package com.karanum.pokeblock.gyms;

import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;

import com.karanum.pokeblock.gyms.files.ConfigFile;
import com.karanum.pokeblock.gyms.files.FileManager;

/**
 * Handles config.yml entries
 * @author Fluctus
 *
 */
public class ConfigManager {

	private boolean fuzzyMatching;
	private int fuzzyPlayerThreshold;
	private int fuzzyGymThreshold;
	private int gymsCooldown;
	private boolean gymsRechallenge;
	
	private ConfigFile configFile;
	
	public ConfigManager() {
		FileManager fileManager = PokeBlockGyms.instance.getFileManager().get();
		configFile = fileManager.loadConfig("config.yml");
		if (configFile == null) {
			PokeBlockGyms.instance.getLogger().log(Level.SEVERE,
					"Could not load configuration file \"config.yml\"!");
		}
		
		YamlConfiguration config = configFile.getConfig();
		fuzzyMatching = config.getBoolean("enable-fuzzy-matching");
		fuzzyPlayerThreshold = config.getInt("fuzzy-matching-player-threshold");
		fuzzyGymThreshold = config.getInt("fuzzy-matching-gym-threshold");
		gymsCooldown = config.getInt("gyms-cooldown-minutes");
		gymsRechallenge = config.getBoolean("gyms-can-be-rechallenged");
		
		PokeBlockGyms.instance.getLogger().log(Level.INFO, 
				"General config file has been loaded!");
	}
	
	public boolean getFuzzyMatching() {
		return fuzzyMatching;
	}
	
	public int getFuzzyPlayerThreshold() {
		return fuzzyPlayerThreshold;
	}
	
	public int getFuzzyGymThreshold() {
		return fuzzyGymThreshold;
	}
	
	/**
	 * @return Gyms cool-down in milliseconds
	 */
	public int getGymsCooldown() {
		return gymsCooldown * 60000;
	}
	
	public boolean getGymsRechallenge() {
		return gymsRechallenge;
	}
	
	public String getColor(String color) {
		if (!configFile.getConfig().contains("colors." + color)) {
			return "f";
		}
		String c = configFile.getConfig().getString("colors." + color);
		return c.substring(0, 0);
	}
	
}

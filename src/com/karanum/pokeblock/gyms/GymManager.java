package com.karanum.pokeblock.gyms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;

import org.bukkit.entity.Player;

import com.karanum.pokeblock.gyms.files.ConfigFile;
import com.karanum.pokeblock.gyms.files.FileManager;

public class GymManager {
	
	private HashMap<String, Gym> gyms;
	
	public GymManager() {
		FileManager fileManager = PokeBlockGyms.instance.getFileManager().get();
		ConfigFile config = fileManager.loadConfig("gyms.yml");
		if (config == null) {
			PokeBlockGyms.instance.getLogger().log(Level.SEVERE,
					"Could not load configuration file \"gyms.yml\"!");
			return;
		}
		
		gyms = new HashMap<String, Gym>();
		for (String gym : config.getConfig().getStringList("gyms")) {
			if (gym.toLowerCase().equals("gyms") || gym.toLowerCase().equals("rules")) {
				PokeBlockGyms.instance.getLogger().log(Level.WARNING,
						"Invalid gym name '" + gym.toLowerCase() + "', skipping...");
				continue;
			}
			gyms.put(gym.toLowerCase(), new Gym(gym.toLowerCase(), config));
		}
		PokeBlockGyms.instance.getLogger().log(Level.INFO, 
				String.format("%d gyms have been loaded!", gyms.keySet().size()));
	}
	
	public Gym getGym(String name) {
		return gyms.get(name.toLowerCase());
	}
	
	public Collection<Gym> getGymList() {
		return gyms.values();
	}
	
	public Collection<String> getGymNames() {
		return gyms.keySet();
	}
	
	public Collection<Gym> leaderOf(Player p) {
		Collection<Gym> result = new ArrayList<Gym>();
		for (Gym gym : gyms.values()) {
			if (gym.isLeader(p)) {
				result.add(gym);
			}
		}
		return result;
	}
	
	public boolean isLeader(Player p) {
		return !leaderOf(p).isEmpty();
	}

}
